package io.itmatic.tracktrot.Fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import io.itmatic.tracktrot.CommonClass.Resource;
import io.itmatic.tracktrot.MainActivity;
import io.itmatic.tracktrot.R;


public class SelectOptionFragment extends Fragment {

    private TextView textGuide;
    private TextView textTraveller;




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_select_option, container, false);

        ((MainActivity) getActivity()).notification.setVisibility(View.GONE);
        ((MainActivity) getActivity()).textTitle.setText(R.string.select_one_option);
        textGuide=(TextView) view.findViewById(R.id.txt_guide);
        textTraveller=(TextView) view.findViewById(R.id.txt_traveller);
        ((MainActivity) getActivity()).notification.setVisibility(View.GONE);
        ((MainActivity) getActivity()).list.setVisibility(View.GONE);
        textGuide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Resource.WHO="guide";

                ((MainActivity) getActivity()).replaceFragment(new LoginFragment());

            }
        });


        textTraveller.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Resource.WHO="user";

                ((MainActivity) getActivity()).replaceFragment(new LoginFragment());

            }
        });
        return view;

    }




}
