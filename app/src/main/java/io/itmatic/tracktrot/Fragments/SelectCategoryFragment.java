package io.itmatic.tracktrot.Fragments;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import io.itmatic.tracktrot.CommonClass.BaseActivity;
import io.itmatic.tracktrot.CommonClass.Resource;
import io.itmatic.tracktrot.MainActivity;
import io.itmatic.tracktrot.Model.Category;
import io.itmatic.tracktrot.R;
import io.itmatic.tracktrot.volley.VolleySingleton;

/**
 * A simple {@link Fragment} subclass.
 */
public class SelectCategoryFragment extends Fragment {


    private ProgressDialog dialog;
    private StringRequest LastSignRequest=null;
    private TextView textCategory;
    private TextView textBlind;

    public SelectCategoryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_choose_category, container, false);
             textCategory=(TextView) view.findViewById(R.id.txt_category);
        ((MainActivity) getActivity()).list.setVisibility(View.GONE);
        textBlind=(TextView) view.findViewById(R.id.txt_blind);

        textCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ((MainActivity) getActivity()).replaceFragment(new CategoryFragment());
            }
        });

        return  view;
    }






}
