package io.itmatic.tracktrot.CommonClass;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Map;

import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.entity.mime.MultipartEntityBuilder;

/**
 * Created by hp on 5/9/2016.
 */
public class MultiPartRequest extends Request<String> {
    public static final String KEY_PICTURE = "mypicture";
    public static final String KEY_PICTURE_NAME = "filename";
    public static final String KEY_ROUTE_ID = "route_id";

    private HttpEntity mHttpEntity;
    private Map<String,String> mHeaders;
    private Map<String,File> mHeader2;
    private String mRouteId;
    private MultipartEntityBuilder builder = MultipartEntityBuilder.create();
    private final Response.Listener<String> mListener;


    public MultiPartRequest(String url, String filePath,
                            Response.Listener<String> listener,
                            Response.ErrorListener errorListener) {
        super(Method.POST, url, errorListener);


        mListener = listener;
      // buildMultipartEntity(filePath);
    }

    public MultiPartRequest(String url, File file, Map header, Map header2,
                            Response.Listener<String> listener,
                            Response.ErrorListener errorListener) {
        super(Method.POST, url, errorListener);
        mHeader2=header2;
        mHeaders = header;
        mListener = listener;


        buildMultipartEntity(file);
    }

   /* private HttpEntity buildMultipartEntity(String filePath) {
        File file = new File(filePath);
        return buildMultipartEntity(file);
    }*/

    private void buildMultipartEntity(File file) {
     //   MultipartEntityBuilder builder = MultipartEntityBuilder.create();
       /* for (Map.Entry<String, File> entry : mHeader2.entrySet()) {
          builder.addBinaryBody(entry.getKey(),entry.getValue());
        }*/

        if(file==null)
        {
            buildMultipartEntitystring();
        }
        else {
            builder.addBinaryBody("file", file);
            mHttpEntity=builder.build();
            buildMultipartEntitystring();
        }
       /* String fileName = file.getName();
        builder.addBinaryBody(KEY_PICTURE, file, ContentType.create("image/jpeg"), fileName);*/


    }

    private void  buildMultipartEntitystring() {
        for (Map.Entry<String, String> entry : mHeaders.entrySet()) {
            builder.addTextBody(entry.getKey(), entry.getValue());
        }
        mHttpEntity=builder.build();
    }


    @Override
    public String getBodyContentType() {
        return mHttpEntity.getContentType().getValue();
    }

    @Override
    public byte[] getBody() throws AuthFailureError {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            mHttpEntity.writeTo(bos);
        } catch (IOException e) {
            VolleyLog.e("IOException writing to ByteArrayOutputStream");
        }
        return bos.toByteArray();
    }

    @Override
    protected Response<String> parseNetworkResponse(NetworkResponse response) {

        String jsonString = "";
        try {
            jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return Response.success(jsonString, getCacheEntry());


    }


    @Override
    protected void deliverResponse(String response) {
        mListener.onResponse(response);
    }
}
