package io.itmatic.tracktrot.Adapters;

import android.content.Context;
import android.content.res.Resources;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import io.itmatic.tracktrot.Fragments.ActivityFragment;
import io.itmatic.tracktrot.Model.Category;
import io.itmatic.tracktrot.R;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ContactViewHolder> { // recycledapter is a type of adapter  it also include recycle

    private List<Category> cat_list;
    private int selectedNo;
    private ActivityFragment fragment;


    public CategoryAdapter(List<Category> contactList,ActivityFragment activityFragment) {
        this.cat_list = contactList;
        this.fragment=activityFragment;
    }

    @Override
    public int getItemCount() {
        return cat_list.size();
    }

    View.OnClickListener getClickListener(final int position, final TextView textname) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // ((MainActivity) getActivity()).replaceFragment(new MapViewFragment());
                Category category = cat_list.get(position);
                if (category.getSelected_status() == 1) {
                    category.setSelected_status(0);
                    textname.setBackgroundColor(fragment.getActivity().getResources().getColor(R.color.blue));
                    selectedNo++;
                } else {
                    category.setSelected_status(1);
                    selectedNo--;
                    textname.setBackgroundColor(fragment.getActivity().getResources().getColor(R.color.white));
                }



                if (selectedNo == 0) {

                    fragment.showaddImage();

                } else {

                    fragment.showremoveImage();
                }

            }
        };
    }


    @Override
    public void onBindViewHolder(ContactViewHolder contactViewHolder, int i) {
        Category ci = cat_list.get(i);

        contactViewHolder.text_name.setText(ci.getName());
        contactViewHolder.text_name.setOnClickListener(getClickListener(i, contactViewHolder.text_name));
        //set a category name on largeplayer


    }

    @Override
    public ContactViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.categorynamelayout, viewGroup, false);

        return new ContactViewHolder(itemView);
    }

    public class ContactViewHolder extends RecyclerView.ViewHolder {
        private TextView text_name;


        public ContactViewHolder(View v) {
            super(v);

            text_name = (TextView) v.findViewById(R.id.txt_catname);


        }
    }

}
