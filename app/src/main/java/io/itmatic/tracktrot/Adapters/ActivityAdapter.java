package io.itmatic.tracktrot.Adapters;

import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import io.itmatic.tracktrot.CommonClass.Resource;
import io.itmatic.tracktrot.Fragments.ActivityFragment;
import io.itmatic.tracktrot.Fragments.EditActivityFragment;
import io.itmatic.tracktrot.MainActivity;
import io.itmatic.tracktrot.Model.Activities;
import io.itmatic.tracktrot.R;

public class ActivityAdapter extends RecyclerView.Adapter<ActivityAdapter.ContactViewHolder> { // recycledapter is a type of adapter  it also include recycle

    private List<Activities> act_list;
    private ActivityFragment fragment;


    public ActivityAdapter(List<Activities> contactList, ActivityFragment activityFragment) {
        this.act_list = contactList;
        this.fragment = activityFragment;

    }

    @Override
    public int getItemCount() {
        return act_list.size();
    }


    View.OnClickListener getClickListener(final int position) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                PopupMenu popup = new PopupMenu(fragment.getActivity(), view);
                //Inflating the Popup using xml file
                popup.getMenuInflater().inflate(R.menu.popup_menu, popup.getMenu());

                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        if (item.getItemId() == R.id.edit) {
                            Resource.isEdit = true;
                            ((MainActivity) fragment.getActivity()).replaceFragment(new EditActivityFragment());
                        } else if (item.getItemId() == R.id.delete) {
                            fragment.deletepachage(Resource.activity.getId());
                        }
                        // Toast.makeText(getActivity(),"You Clicked : " + item.getTitle(),Toast.LENGTH_SHORT).show();
                        return true;
                    }
                });

                popup.show();//showing popup menu


            }
        };
    }

    @Override
    public void onBindViewHolder(ContactViewHolder contactViewHolder, int i) {
        Activities ci = act_list.get(i);

        //  contactViewHolder.text_name.setText(ci.getName());
        // contactViewHolder.imgMenu.setOnClickListener(getClickListener(i));
        contactViewHolder.textActivity.setText(ci.getActivityName());
        contactViewHolder.textTime.setText(ci.getTime());
        String rate = (ci.getRate());
        contactViewHolder.textRate.setText(rate);
        contactViewHolder.imgMenu.setOnClickListener(getClickListener(i));
        //set a category name on largeplayer


    }

    @Override
    public ContactViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.activitieslayout, viewGroup, false);

        return new ContactViewHolder(itemView);
    }


    public class ContactViewHolder extends RecyclerView.ViewHolder {
        private TextView textActivity;
        private TextView textTime;
        private TextView textRate;
        private ImageView imgMenu;
        private int itemId;


        public ContactViewHolder(View v) {
            super(v);

            // text_name=(TextView) v.findViewById(R.id.);
            textActivity = (TextView) v.findViewById(R.id.txt_activity);
            textTime = (TextView) v.findViewById(R.id.txt_time);
            textRate = (TextView) v.findViewById(R.id.txt_rate);
            imgMenu = (ImageView) v.findViewById(R.id.img_menu);
            // imgMenu.setOnCreateContextMenuListener(this);


        }


    }


}
