package io.itmatic.tracktrot;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.UserRecoverableAuthException;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import io.itmatic.tracktrot.CommonClass.BaseActivity;
import io.itmatic.tracktrot.CommonClass.RealPathUtil;
import io.itmatic.tracktrot.CommonClass.Resource;
import io.itmatic.tracktrot.Fragments.CategoryFragment;
import io.itmatic.tracktrot.Fragments.EditActivityFragment;
import io.itmatic.tracktrot.Fragments.EnterOtpFragment;
import io.itmatic.tracktrot.Fragments.LoginFragment;
import io.itmatic.tracktrot.Fragments.MapViewFragment;
import io.itmatic.tracktrot.Fragments.SelectCategoryFragment;
import io.itmatic.tracktrot.Fragments.SelectOptionFragment;
import io.itmatic.tracktrot.Fragments.SignUpFragment;
import io.itmatic.tracktrot.Fragments.UpLoadImageFragment;
import io.itmatic.tracktrot.Fragments.UploadGuideImages;
import io.itmatic.tracktrot.Fragments.VarifyMobileFragment;
import io.itmatic.tracktrot.volley.VolleySingleton;

public class MainActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {


    public FragmentManager fragmentManager;
    private FragmentTransaction fragmentTransaction;
    private SharedPreferences preferences;
    private SharedPreferences.Editor prefEditor;
    private Boolean login;
    public TextView textTitle;
    private NavigationView navigationView;
    public Toolbar toolbar;
    private String regId;
    public LinearLayout notification;
    public LinearLayout list;
    private DrawerLayout drawer;
    private StringRequest LastSignRequest = null;
    private ConnectionResult mConnectionResult;
    CallbackManager callbackManager;
    private ProgressDialog dialog;
    private static final String TAG = "RetrieveAccessToken";
    private static final int REQ_SIGN_IN_REQUIRED = 55664;
    private final String[] sInitialPerms = {
            Manifest.permission.GET_ACCOUNTS
    };

    private final int iInitialRequest = 1337;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

      /*  NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);*/

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        textTitle = (TextView) findViewById(R.id.toolbar_title);
        notification=(LinearLayout) findViewById(R.id.lout_notification);
        list=(LinearLayout) findViewById(R.id.lout_list);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        FacebookSdk.sdkInitialize(this);
        callbackManager = CallbackManager.Factory.create();
        Resource.mGoogleApiClient = new GoogleApiClient.Builder(MainActivity.this).addConnectionCallbacks(MainActivity.this).addOnConnectionFailedListener(MainActivity.this).addApi(Plus.API, Plus.PlusOptions.builder().build()).addScope(Plus.SCOPE_PLUS_LOGIN).build();
        list.setVisibility(View.GONE);



        /*
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();*/


        preferences = MainActivity.this.getSharedPreferences(getPackageName(), Context.MODE_PRIVATE);
       // registerInBackground();
        if (preferences.contains("token")) {
            String token = preferences.getString("token", "");
            String who = preferences.getString("who", "");
            Resource.TOKEN = token;
            Resource.WHO = who;
            getprofile(token);
        } else {

            replaceFragment(new SelectOptionFragment());

        }


    }

    private boolean canAccessGetAccount() {
        return (hasPermission(Manifest.permission.GET_ACCOUNTS));
    }


    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ContextCompat.checkSelfPermission(this, perm));
    }


    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {

        if (requestCode == 64206 && resultCode == -1) {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        } else if (requestCode == 65543 && resultCode == RESULT_OK) {
            // sendfileondropboxbycamera(data, "Uploadingfile...");
            // Toast.makeText(OptionalContactInfo.this, selectedImagePath, Toast.LENGTH_LONG).show();

            Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
            Fragment f = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
            if (Resource.IMAGETYPE == 1)
                ((UploadGuideImages) f).getImage(thumbnail);
            else if (Resource.IMAGETYPE == 2)
                ((UploadGuideImages) f).setimageproof1(thumbnail);
            else if (Resource.IMAGETYPE == 3)
                ((UploadGuideImages) f).setimageproof2(thumbnail);
            else if(Resource.IMAGETYPE==4)
                ((UpLoadImageFragment) f).getImage(thumbnail);
            //mImage.setImageBitmap(thumbnail);
            //3
            /*ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            thumbnail.compress(Bitmap.CompressFormat.JPEG, 100, bytes);*/
        } else if ((requestCode == 65542 || requestCode == 131078 || requestCode == 393222 || requestCode == 393222 || requestCode == 262150) && resultCode == RESULT_OK) {
            String realPath;
            // SDK < API11
            if (Build.VERSION.SDK_INT < 11)
                realPath = RealPathUtil.getRealPathFromURI_BelowAPI11(this, data.getData());

                // SDK >= 11 && SDK < 19
            else if (Build.VERSION.SDK_INT < 19)
                realPath = RealPathUtil.getRealPathFromURI_API11to18(this, data.getData());

                // SDK > 19 (Android 4.4)
            else
                realPath = RealPathUtil.getPath(this, data.getData());


            //Uri selectedImageUri = data.getData();
            final File file = new File(realPath);

            if (file.exists()) {
                Bitmap myBitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
                Fragment f = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
                if (Resource.IMAGETYPE == 1)
                    ((UploadGuideImages) f).getImage(myBitmap);
                else if (Resource.IMAGETYPE == 2)
                    ((UploadGuideImages) f).setimageproof1(myBitmap);
                else if (Resource.IMAGETYPE == 3)
                    ((UploadGuideImages) f).setimageproof2(myBitmap);
                else if(Resource.IMAGETYPE==4)
                    ((UpLoadImageFragment) f).getImage(myBitmap);

            }

        } else if (requestCode == 1 && resultCode == RESULT_OK) {

            if (!Resource.mGoogleApiClient.isConnecting()) {
                Resource.mGoogleApiClient.connect();


            } else {
                getProfileInformation();
            }
        }
        else if(resultCode==0)
        {
            dialog.dismiss();

        }
    }

    private void resolveSignInError() {
        if (mConnectionResult.hasResolution()) {
            try {

                mConnectionResult.startResolutionForResult(this, 1);
            } catch (IntentSender.SendIntentException e) {

                Resource.mGoogleApiClient.connect();
            }
        }
    }

    @Override
    public void onConnected(Bundle bundle) {


        getProfileInformation();

        // Get user's information


    }


    protected void onStop() {
        super.onStop();
        if (Resource.mGoogleApiClient.isConnected()) {
            Resource.mGoogleApiClient.disconnect();

        }
    }


    @Override
    public void onConnectionSuspended(int i) {
        Resource.mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

        if (!connectionResult.hasResolution()) {
            GooglePlayServicesUtil.getErrorDialog(connectionResult.getErrorCode(), this,
                    0).show();
            return;
        }


        // Store the ConnectionResult for later usage
        mConnectionResult = connectionResult;


        // The user has already clicked 'sign-in' so we attempt to
        // resolve all
        // errors until the user is signed in, or they cancel.
        resolveSignInError();


    }


    private void getProfileInformation() {
        try {
            if (Plus.PeopleApi.getCurrentPerson(Resource.mGoogleApiClient) != null) {


                new RetrieveTokenTask().execute();


              /*  Intent intent=new Intent(Register.this,UserInfo.class);
                intent.putExtra(JsonKey.EMAIL,email);
                intent.putExtra(JsonKey.FIRST_NAME,personName);*/

                //  startActivity(intent);


            } else {
                dialog.dismiss();

                Toast.makeText(this,
                        getResources().getString(R.string.person_info_null), Toast.LENGTH_LONG).show();


            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class RetrieveTokenTask extends AsyncTask<String, Void, String> {


        @Override
        protected String doInBackground(String... params) {
            Person currentPerson = Plus.PeopleApi.getCurrentPerson(Resource.mGoogleApiClient);
            String personName = currentPerson.getDisplayName();

            String scopes = "oauth2:profile email";
            String personPhotoUrl = currentPerson.getImage().getUrl();
            String personGooglePlusProfile = currentPerson.getUrl();

            String email = Plus.AccountApi.getAccountName(Resource.mGoogleApiClient);
            String token = null;

            try {
                token = GoogleAuthUtil.getToken(getApplicationContext(), email, scopes);
                Plus.AccountApi.clearDefaultAccount(Resource.mGoogleApiClient);
                Resource.mGoogleApiClient.disconnect();
            } catch (IOException e) {
                Log.e(TAG, e.getMessage());
            } catch (UserRecoverableAuthException e) {
                startActivityForResult(e.getIntent(), REQ_SIGN_IN_REQUIRED);
            } catch (GoogleAuthException e) {
                Log.e(TAG, e.getMessage());
            }
            return token;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            googlelogin(s);
        }
    }

    @Override
    public void onBackPressed() {
        Fragment f = getSupportFragmentManager().findFragmentById(R.id.fragment_container);



        if (f instanceof SelectOptionFragment||f instanceof VarifyMobileFragment||f instanceof VarifyMobileFragment|| f instanceof UploadGuideImages ||f instanceof UpLoadImageFragment || fragmentManager==null || f==null || fragmentManager.getBackStackEntryCount()==1) {
            finish();
        }
        else if(f instanceof LoginFragment)
        {
            Resource.mGoogleApiClient.disconnect();
        }
        else {

            fragmentManager.popBackStackImmediate();
            Fragment t = getSupportFragmentManager().findFragmentById(R.id.fragment_container);

            if(t instanceof SelectOptionFragment)
            {
                textTitle.setText(R.string.select_one_option);
            }
            else if(t instanceof LoginFragment)
            {
                textTitle.setText(R.string.sign_in);
            }
            else if(t instanceof SignUpFragment)
            {
                textTitle.setText(R.string.sign_up);
            }
            else if(t instanceof SelectCategoryFragment)
            {
                textTitle.setText(R.string.select_category);
            }
            else if(t instanceof VarifyMobileFragment)
            {
                textTitle.setText(R.string.VerifyMobile);
            }

            else if(t instanceof EnterOtpFragment)
            {
                textTitle.setText(R.string.enter_code);
            }

            else if(t instanceof EditActivityFragment)
            {
                textTitle.setText(R.string.add_activity);
            }


        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /*************************************************************************************************************************************************/
    public void replaceFragment(Fragment fragment) {
        try {
            fragmentManager = getSupportFragmentManager();
            fragmentTransaction = fragmentManager.beginTransaction();
            String name = fragment.toString();
            fragmentTransaction.remove(fragment);
            fragmentTransaction.add(R.id.fragment_container, fragment).addToBackStack(name);
            fragmentTransaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*******************************************************************************************************************************************/
    public void showdrawer() {

        navigationView.setVisibility(View.VISIBLE);
        navigationView.setNavigationItemSelectedListener(this);


        drawer.setVisibility(View.VISIBLE);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
    }

    public void loginwithgoogle(boolean signin) {
        login = signin;
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.GET_ACCOUNTS) == PackageManager.PERMISSION_GRANTED) {

            dialog = BaseActivity.ShowConstantProgressNOTCAN(this, "", getResources().getString(R.string.process_sing_up));
            dialog.show();

            Resource.mGoogleApiClient.connect();
        } else {
            ActivityCompat.requestPermissions(this, sInitialPerms, iInitialRequest);
        }
    }

    /****************************************************************************************************************************************/
    public void loginwithfacebook(final boolean signin) {


        List<String> permissionNeeds = Arrays.asList("user_photos", "email", "user_birthday", "user_friends");
        //  super.onCreate(savedInstanceState);

        LoginManager.getInstance().logInWithReadPermissions(
                this,
                permissionNeeds);
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResults) {
                        //  final FacebookDetail fbdetail = new FacebookDetail();
                        AccessToken accessToken = AccessToken.getCurrentAccessToken();
                        final String fbToken = accessToken.getToken();
                        GraphRequest request = GraphRequest.newMeRequest(
                                loginResults.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(
                                            JSONObject object,
                                            GraphResponse response) {

                                        facebooklogin(fbToken,signin);



                                      /*  fbdetail.setEmail(object.optString("email"));
                                        fbdetail.setFullname(object.optString("name"));
                                        fbdetail.setId(object.optInt("id"));*/

                                    }
                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,name,email,gender, birthday");
                        request.setParameters(parameters);
                        request.executeAsync();


                    }

                    @Override
                    public void onCancel() {



                    }


                    @Override
                    public void onError(FacebookException e) {




                    }
                });
    }

    /******************************************************************************************************************************************/
    private void facebooklogin(String fbtoken, final boolean signin) {
        dialog = BaseActivity.ShowConstantProgressNOTCAN(this, "", getResources().getString(R.string.processing));
        dialog.show();

        final RequestQueue queue = VolleySingleton.getInstance(this).getmRequestQueue();
        String url = (Resource.BASE_URL + Resource.WHO + "/facebook/connect.json?access_token=" + fbtoken);
        StringRequest fb_req = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                dialog.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response);  // response is a array come from API

                    boolean success = jsonObject.getBoolean("success");
                    if (success) {
                        JSONObject data = jsonObject.getJSONObject("response");
                        String utoken = data.getString("access_token");
                        int signupprocess = data.getInt("sign_up_process_id");
                        prefEditor = getSharedPreferences(getPackageName(), Context.MODE_PRIVATE).edit();
                        prefEditor.putString("token", utoken);
                        prefEditor.putString("who", Resource.WHO);
                        Resource.TOKEN = utoken;
                        prefEditor.commit();
                        if (Resource.WHO.equals("user")) {
                            switch (signupprocess) {
                                case 0:
                                    replaceFragment(new SignUpFragment());
                                    break;
                                case 1:
                                    replaceFragment(new VarifyMobileFragment());
                                    break;
                                case 2:
                                    replaceFragment(new UpLoadImageFragment());
                                    break;
                                case 3:
                                    replaceFragment(new MapViewFragment());
                                    break;
                            }


                        } else {
                            switch (signupprocess) {
                                case 0:
                                    replaceFragment(new SignUpFragment());
                                    break;
                                case 1:
                                    replaceFragment(new VarifyMobileFragment());
                                    break;
                                case 2:
                                    replaceFragment(new UploadGuideImages());
                                    break;
                                case 3:
                                    replaceFragment(new CategoryFragment());
                                    break;
                                case 4:
                                    replaceFragment(new SelectCategoryFragment());
                                    break;
                            }

                        }
                    }else {
                        JSONObject error = jsonObject.getJSONObject("error");
                        String message = error.getString("description");
                        BaseActivity.showerror(MainActivity.this, message);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    dialog.dismiss();
                }


                //obj.register();

                // Toast.makeText(EWalletApplication.getContext(), " Register", Toast.LENGTH_SHORT).show();

            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                dialog.dismiss();
                //showerror("not set gcm id on server");

                Toast.makeText(MainActivity.this, getResources().getString(R.string.facebook_login_failed), Toast.LENGTH_SHORT).show();


            }

        }) {
            @Override
            public HashMap<String, String> getParams() {
                HashMap<String, String> params = new HashMap<String, String>();


                return params;
            }
        };

        fb_req.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(fb_req);


    }

    /*************************************************************************************************************************************/
    public void register(final String name, final String email, final String password) {
        dialog = BaseActivity.ShowConstantProgressNOTCAN(this, "", getResources().getString(R.string.process_sing_up));
        dialog.show();

        final RequestQueue queue = VolleySingleton.getInstance(this).getmRequestQueue();


        String url = (Resource.BASE_URL + Resource.WHO + "/register.json");

        StringRequest signuprequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                dialog.dismiss();

                try {

                    JSONObject jsonObject = new JSONObject(response);  // response is a array come from API

                    boolean success = jsonObject.getBoolean("success");
                    if (success) {

                        JSONObject data = jsonObject.getJSONObject("response");

                        String token = data.getString("access_token");
                        String email = data.getString("email");
                        prefEditor = getSharedPreferences(getPackageName(), Context.MODE_PRIVATE).edit();
                        prefEditor.putString("token", token);
                        prefEditor.putString("who", Resource.WHO);
                        Resource.TOKEN = token;
                        prefEditor.commit();
                        replaceFragment(new VarifyMobileFragment());


                    } else {
                        JSONObject err = jsonObject.getJSONObject("error");
                        String errormessage = err.getString("description");
                        BaseActivity.showerror(MainActivity.this, errormessage);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    dialog.dismiss();
                }


                //Toast.makeText(EWalletApplication.getContext(), "login", Toast.LENGTH_SHORT).show();

            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError err) {
                dialog.dismiss();

                BaseActivity.showerror(MainActivity.this,getResources().getString(R.string.server_error));

                // Toast.makeText(EWalletApplication.getContext(), " Not Register", Toast.LENGTH_SHORT).show();


            }

        }) {
            @Override
            public HashMap<String, String> getParams() {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("firstName", name);
                params.put("email", email);
                params.put("password", password);


                return params;
            }


        };

        signuprequest.setRetryPolicy(new DefaultRetryPolicy(10000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        if (LastSignRequest != null && !LastSignRequest.hasHadResponseDelivered()) {
            LastSignRequest.cancel();
        }

        LastSignRequest = signuprequest;
        signuprequest.setShouldCache(false);
        queue.add(signuprequest);


    }

    /*******************************************************************************************************************************************/
    private void registerInBackground() {
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                String msg = "";
                try {
                 /*   if (gcmObj == null) {
                        gcmObj = GoogleCloudMessaging.getInstance(Sign_In.this);
                    }
                    regId = gcmObj.register(Resource.GOOGLE_PROJ_ID);
                    msg = "Registration ID :" + regId;*/

                    InstanceID instanceID = InstanceID.getInstance(MainActivity.this);
                    regId = instanceID.getToken("873813873875", GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);


                } catch (IOException ex) {
                    //msg = "Error :" + ex.getMessage();
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
                if (!TextUtils.isEmpty(regId)) {


                    //facebooklogin(token,regId);

                    // Store RegId created by GCM Server in SharedPref
                  /*  storeRegIdinSharedPref(Sign_In.this, regId, emailID);
                    Toast.makeText(Sign_In.this,"Registered with GCM Server successfully.nn" + msg, Toast.LENGTH_SHORT).show();*/
                } else {
                    dialog.dismiss();
                    Toast.makeText(MainActivity.this,getResources().getString(R.string.gcm_error)  + msg, Toast.LENGTH_LONG).show();
                }
            }
        }.execute(null, null, null);
    }

/**********************************************************************************************************************************************/


    /**********************************************************************************************************************************/
    public void login(final String email, final String pass) {

        dialog = BaseActivity.ShowConstantProgressNOTCAN(this, "", "SignIn....");
        dialog.show();

        final RequestQueue queue = VolleySingleton.getInstance(this).getmRequestQueue();


        String url = (Resource.BASE_URL + Resource.WHO + "/login.json");

        StringRequest stringRequest_sign_up = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                dialog.dismiss();
                try {

                    JSONObject jsonObject = new JSONObject(response);  // response is a array come from API

                    boolean success = jsonObject.getBoolean("success");
                    if (success) {

                        JSONObject data = jsonObject.getJSONObject("response");

                        String token = data.getString("access_token");
                        String email = data.getString("email");
                        prefEditor = getSharedPreferences(getPackageName(), Context.MODE_PRIVATE).edit();
                        prefEditor.putString("token", token);
                        prefEditor.putString("who", Resource.WHO);
                        Resource.TOKEN = token;
                        prefEditor.commit();

                        if (Resource.WHO.equals("user"))
                            replaceFragment(new CategoryFragment());
                        else
                            replaceFragment(new MapViewFragment());


                    } else {
                        JSONObject err = jsonObject.getJSONObject("error");
                        String errormessage = err.getString("description");
                        BaseActivity.showerror(MainActivity.this, errormessage);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


                //Toast.makeText(EWalletApplication.getContext(), "login", Toast.LENGTH_SHORT).show();

            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError err) {
                dialog.dismiss();

                BaseActivity.showerror(MainActivity.this,getResources().getString(R.string.server_error));

                // Toast.makeText(EWalletApplication.getContext(), " Not Register", Toast.LENGTH_SHORT).show();


            }

        }) {
            @Override
            public HashMap<String, String> getParams() {
                HashMap<String, String> params = new HashMap<String, String>();

                params.put("email_phone", email);
                params.put("password", pass);


                return params;
            }


        };

        stringRequest_sign_up.setRetryPolicy(new DefaultRetryPolicy(10000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        if (LastSignRequest != null && !LastSignRequest.hasHadResponseDelivered()) {
            LastSignRequest.cancel();
        }

        LastSignRequest = stringRequest_sign_up;
        stringRequest_sign_up.setShouldCache(false);
        queue.add(stringRequest_sign_up);
    }


    /**********************************************************************************************************************************/

    private void googlelogin(String gtoken) {

        Resource.mGoogleApiClient.disconnect();

        final RequestQueue queue = VolleySingleton.getInstance(this).getmRequestQueue();
        String url = (Resource.BASE_URL + Resource.WHO + "/google/connect.json?access_token=" + gtoken);
        StringRequest stringRequest_google_login = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                dialog.dismiss();

                try {
                    JSONObject jsonObject = new JSONObject(response);  // response is a array come from API

                    boolean success = jsonObject.getBoolean("success");

                    if (success) {
                        JSONObject data = jsonObject.getJSONObject("response");
                        String utoken = data.getString("access_token");
                        int signupprocess=data.getInt("sign_up_process_id");
                        prefEditor = getSharedPreferences(getPackageName(), Context.MODE_PRIVATE).edit();
                        prefEditor.putString("token", utoken);
                        prefEditor.putString("who", Resource.WHO);
                        Resource.TOKEN = utoken;
                        prefEditor.commit();
                        if (Resource.WHO.equals("user")) {
                            switch (signupprocess)
                            {
                                case 0:
                                    replaceFragment(new SignUpFragment());
                                    break;
                                case 1:
                                    replaceFragment(new VarifyMobileFragment());
                                    break;
                                case 2:
                                    replaceFragment(new UpLoadImageFragment());
                                    break;
                                case 3:
                                    replaceFragment(new SelectCategoryFragment());
                                    break;
                            }


                        } else {
                            switch (signupprocess)
                            {
                                case 0:
                                    replaceFragment(new SignUpFragment());
                                    break;
                                case 1:
                                    replaceFragment(new VarifyMobileFragment());
                                    break;
                                case 2:
                                    replaceFragment(new UploadGuideImages());
                                    break;
                                case 3:
                                    replaceFragment(new CategoryFragment());
                                    break;
                                case 4:
                                    replaceFragment(new MapViewFragment());
                                    break;
                            }



                        }


                      /*  if (login) {
                            if (Resource.WHO.equals("user"))
                                replaceFragment(new CategoryFragment());
                            else
                                replaceFragment(new MapViewFragment());
                        } else
                            replaceFragment(new VarifyMobileFragment());*/
                    } else {
                        JSONObject err = jsonObject.getJSONObject("error");
                        String message = err.getString("description");
                        BaseActivity.showerror(MainActivity.this, message);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


                //obj.register();

                // Toast.makeText(EWalletApplication.getContext(), " Register", Toast.LENGTH_SHORT).show();

            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                dialog.dismiss();
                //showerror("not set gcm id on server");

                Toast.makeText(MainActivity.this, getResources().getString(R.string.google_login_failed), Toast.LENGTH_SHORT).show();


            }

        }) {
            @Override
            public HashMap<String, String> getParams() {
                HashMap<String, String> params = new HashMap<String, String>();


                return params;
            }
        };

        stringRequest_google_login.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(stringRequest_google_login);


    }

    /**************************************************************************************************************************************/

    private void getprofile(String utoken) {

        final RequestQueue queue = VolleySingleton.getInstance(this).getmRequestQueue();
        String url = (Resource.BASE_URL + Resource.WHO + "/profile.json?token=" + utoken);
        StringRequest stringRequest_profile = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {


                try {
                    JSONObject jsonObject = new JSONObject(response);  // response is a array come from API

                    boolean success = jsonObject.getBoolean("success");
                    if (success) {
                        JSONObject data = jsonObject.getJSONObject("response");
                        String utoken = data.getString("access_token");
                        int signupprocess = data.getInt("sign_up_process_id");

                        prefEditor = getSharedPreferences(getPackageName(), Context.MODE_PRIVATE).edit();
                        prefEditor.putString("token", utoken);
                        prefEditor.putString("who", Resource.WHO);
                        Resource.TOKEN = utoken;
                        prefEditor.commit();
                        if (Resource.WHO.equals("user")) {
                            switch (signupprocess)
                            {
                                case 0:
                                    replaceFragment(new SignUpFragment());
                                    break;
                                case 1:
                                    replaceFragment(new VarifyMobileFragment());
                                    break;
                                case 2:
                                    replaceFragment(new UpLoadImageFragment());
                                    break;
                                case 3:
                                    replaceFragment(new SelectCategoryFragment());
                                    break;
                            }


                        } else {
                            switch (signupprocess)
                            {
                                case 0:
                                    replaceFragment(new SignUpFragment());
                                    break;
                                case 1:
                                    replaceFragment(new VarifyMobileFragment());
                                    break;
                                case 2:
                                    replaceFragment(new UploadGuideImages());
                                    break;
                                case 3:
                                    replaceFragment(new CategoryFragment());
                                    break;
                                case 4:
                                    replaceFragment(new MapViewFragment());
                                    break;
                            }



                        }

                    } else {
                        JSONObject error = jsonObject.getJSONObject("error");
                        String message = error.getString("description");
                        BaseActivity.showerror(MainActivity.this,message);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


                //obj.register();

                // Toast.makeText(EWalletApplication.getContext(), " Register", Toast.LENGTH_SHORT).show();

            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                dialog.dismiss();
                //showerror("not set gcm id on server");

                Toast.makeText(MainActivity.this, getResources().getString(R.string.profile_not_getting), Toast.LENGTH_SHORT).show();


            }

        }) {
            @Override
            public HashMap<String, String> getParams() {
                HashMap<String, String> params = new HashMap<String, String>();


                return params;
            }
        };

        stringRequest_profile.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(stringRequest_profile);


    }


}
