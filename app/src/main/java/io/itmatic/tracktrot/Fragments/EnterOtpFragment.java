package io.itmatic.tracktrot.Fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;

import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import io.itmatic.tracktrot.CommonClass.BaseActivity;
import io.itmatic.tracktrot.CommonClass.Resource;
import io.itmatic.tracktrot.MainActivity;
import io.itmatic.tracktrot.R;
import io.itmatic.tracktrot.volley.VolleySingleton;

public class EnterOtpFragment extends Fragment {

    private TextView textConfirm;
    private EditText editCode;
    private ProgressDialog dialog;
    private TextView textResend;











    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_enter_otp, container, false);

        ((MainActivity) getActivity()).textTitle.setText(R.string.enter_code);
         textConfirm=(TextView) view.findViewById(R.id.txt_confirm);
         editCode=(EditText) view.findViewById(R.id.edt_code);
         textResend=(TextView) view.findViewById(R.id.txt_resend);
        ((MainActivity) getActivity()).notification.setVisibility(View.GONE);
        ((MainActivity) getActivity()).list.setVisibility(View.GONE);
        textConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //((MainActivity) getActivity()).replaceFragment(new UpLoadImageFragment());

                if(editCode.getText().toString()!=null || editCode.getText().toString()!="")
                {
                   setotpcode(editCode.getText().toString());
                }
                else
                {
                     BaseActivity.showerror(getActivity(),getResources().getString(R.string.otperror));
                }
            }
        });

        textResend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(editCode.getText().toString()!=null || editCode.getText().toString()!="")
                {
                    setotpcode(editCode.getText().toString());
                }
                else
                {
                    BaseActivity.showerror(getActivity(),getResources().getString(R.string.otperror));
                }



            }
        });
         Resource.handler1=new Handler() {
             @Override
             public void handleMessage(Message msg) {

                 editCode.setText(Resource.OTPCODE);

             }
         };

        return view;
    }



    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }



    private void setotpcode(final String code) {

        dialog = BaseActivity.ShowConstantProgressNOTCAN(getActivity(), "",getResources().getString(R.string.sendcode));
        dialog.show();

        final RequestQueue queue = VolleySingleton.getInstance(getActivity()).getmRequestQueue();


        String url = (Resource.BASE_URL + "user/otp/verify.json");

        StringRequest stringRequest_sign_up = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                dialog.dismiss();
                try {

                    JSONObject jsonObject = new JSONObject(response);  // response is a array come from API

                    boolean success = jsonObject.getBoolean("success");
                    if (success) {

                         if(Resource.WHO.equals("user")) {
                             ((MainActivity) getActivity()).replaceFragment(new UpLoadImageFragment());
                         }
                        else
                         {
                             ((MainActivity) getActivity()).replaceFragment(new UploadGuideImages());
                         }



                    } else {
                        JSONObject err = jsonObject.getJSONObject("error");
                        String message = err.getString("description");
                        BaseActivity.showerror(getActivity(), message);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


                //Toast.makeText(EWalletApplication.getContext(), "login", Toast.LENGTH_SHORT).show();

            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError err) {
                dialog.dismiss();

                BaseActivity.showerror(getActivity(),getResources().getString(R.string.server_error));

                // Toast.makeText(EWalletApplication.getContext(), " Not Register", Toast.LENGTH_SHORT).show();


            }

        }) {
            @Override
            public HashMap<String, String> getParams() {
                HashMap<String, String> params = new HashMap<String, String>();

                params.put("verification_code",code);
                params.put("token",Resource.TOKEN);

                return params;
            }


        };

        stringRequest_sign_up.setRetryPolicy(new DefaultRetryPolicy(10000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(stringRequest_sign_up);
    }

}
