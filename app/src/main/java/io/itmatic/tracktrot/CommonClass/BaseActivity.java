package io.itmatic.tracktrot.CommonClass;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import io.itmatic.tracktrot.R;

/**
 * Created by Manoj on 11/3/2015.
 */
public  abstract  class BaseActivity extends AppCompatActivity {

    public static ProgressDialog ShowConstantProgressNOTCAN(Context context, String Title, String Message) {
        ProgressDialog dialog = new ProgressDialog(context);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setMax(100);
        dialog.setIndeterminate(false);
        if (Title.equals(null) || Title.equals("")) {

        } else {
            dialog.setTitle(Title);
        }
        dialog.setMessage(Message);
        dialog.setCancelable(false);
        return dialog;
    }
    public static void showerror(Context context,String error)
    {
        new AlertDialog.Builder(context)
                .setTitle("Error")
                .setMessage(error)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        dialog.cancel();
                    }
                })

                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }



    public void onFragmentChange(Context context)
    {

    }

    public void replaceFragment(Fragment fragment)
    {

    }
}


