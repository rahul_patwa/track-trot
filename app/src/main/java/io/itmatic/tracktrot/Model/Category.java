package io.itmatic.tracktrot.Model;

/**
 * Created by Manoj on 8/17/2016.
 */
public class Category {

    private String name;
    private String url;
    private boolean enabled;
    private int id;
    private int selected_status;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSelected_status() {
        return selected_status;
    }

    public void setSelected_status(int selected_status) {
        this.selected_status = selected_status;
    }
}
