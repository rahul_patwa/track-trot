package io.itmatic.tracktrot.Adapters;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import java.util.List;

import io.itmatic.tracktrot.CommonClass.Resource;
import io.itmatic.tracktrot.Fragments.CategoryFragment;
import io.itmatic.tracktrot.Model.Category;
import io.itmatic.tracktrot.R;

public class SetCategoryAdapter extends RecyclerView.Adapter<SetCategoryAdapter.ContactViewHolder> { // recycledapter is a type of adapter  it also include recycle

    private List<Category> cat_list;
    private CategoryFragment fragment;
    private ImageLoader mIMAGELOADER;
    private int selectedNo;

    public SetCategoryAdapter(List<Category> contactList, CategoryFragment categoryFragment, ImageLoader imageLoader) {
        this.cat_list = contactList;
        this.fragment = categoryFragment;
        this.mIMAGELOADER = imageLoader;
    }

    @Override
    public int getItemCount() {
        return cat_list.size();
    }

    View.OnClickListener getClickListener(final int position) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // ((MainActivity) getActivity()).replaceFragment(new MapViewFragment());
                Category category = cat_list.get(position);
                if (category.getSelected_status() == 0) {
                    category.setSelected_status(1);
                    view.setBackgroundColor(fragment.getActivity().getResources().getColor(R.color.blue));
                    selectedNo++;
                } else {
                    category.setSelected_status(0);
                    selectedNo--;
                    view.setBackgroundColor(fragment.getActivity().getResources().getColor(R.color.white));
                }

                if (selectedNo == 0) {

                } else {

                }

            }
        };
    }


    @Override
    public void onBindViewHolder(ContactViewHolder contactViewHolder, int i) {
        Category ci = cat_list.get(i);

        contactViewHolder.text_name.setText(ci.getName());
        contactViewHolder.nv_icon.setDefaultImageResId(R.drawable.user);
        contactViewHolder.nv_icon.setImageUrl(ci.getUrl(), mIMAGELOADER);
        contactViewHolder.cardView.setBackgroundColor(fragment.getActivity().getResources().getColor(R.color.recive));
        contactViewHolder.cardView.setOnClickListener(getClickListener(i));

        for (int j = 0; j < Resource.usercategories.size(); j++) {
            if (ci.getId() == Resource.usercategories.get(j).getId()) {
                ci.setSelected_status(1);
                contactViewHolder.cardView.setBackgroundColor(fragment.getActivity().getResources().getColor(R.color.blue));
                selectedNo++;

            }

        }
        //set a category name on largeplayer


    }

    @Override
    public ContactViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.category_view, viewGroup, false);

        return new ContactViewHolder(itemView);
    }

    public class ContactViewHolder extends RecyclerView.ViewHolder {
        private NetworkImageView nv_icon;
        private TextView text_name;
        private CardView cardView;


        public ContactViewHolder(View v) {
            super(v);
            nv_icon = (NetworkImageView) v.findViewById(R.id.img_catimage);
            text_name = (TextView) v.findViewById(R.id.txt_name);
            cardView = (CardView) v.findViewById(R.id.cardview);


        }
    }

}