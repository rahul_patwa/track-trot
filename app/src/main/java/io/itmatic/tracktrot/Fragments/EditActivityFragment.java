package io.itmatic.tracktrot.Fragments;



import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.itmatic.tracktrot.CommonClass.BaseActivity;
import io.itmatic.tracktrot.CommonClass.Resource;
import io.itmatic.tracktrot.MainActivity;
import io.itmatic.tracktrot.Model.Activities;
import io.itmatic.tracktrot.R;
import io.itmatic.tracktrot.volley.VolleySingleton;

/**
 * A simple {@link Fragment} subclass.
 *
 */
public class EditActivityFragment extends Fragment {

    private Spinner timeSpinner;
    private TextInputLayout textinActivity;
    private TextInputLayout textinRate;
    private EditText editTextActivity;
    private EditText editTextRate;
    private Button btnSave;
    private  boolean isEdit=false;
    private String selectTime;
    private ProgressDialog dialog;
    public EditActivityFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_edit, container, false);
        timeSpinner=(Spinner) view.findViewById(R.id.sp_time);
        textinActivity=(TextInputLayout) view.findViewById(R.id.input_layout_activityname);
        textinRate=(TextInputLayout) view.findViewById(R.id.input_layout_rate);
        editTextActivity=(EditText) view.findViewById(R.id.edt_input_activityname);
        editTextRate=(EditText) view.findViewById(R.id.edt_input_rate);
        ((MainActivity) getActivity()).list.setVisibility(View.GONE);
        btnSave=(Button) view.findViewById(R.id.btn_save);
        timeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                      if(position==0)
                          selectTime="Hourly";
                      else if(position==1)
                          selectTime="Weekly";
                      else if(position==2)
                          selectTime="Monthly";

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String result=checkValidity(editTextActivity,editTextRate);
                if(!result.equals("Success"))
                {
                    // lERROR.setVisibility(View.VISIBLE);

                }
                else
                {
                    if(Resource.isEdit==false)
                    {
                        Resource.addactivity=true;

                        Resource.activity.setActivityName(editTextActivity.getText().toString());
                        Resource.activity.setRate("$"+editTextRate.getText().toString());
                        Resource.activity.setTime(selectTime);
                        addpackage(Resource.activity);


                    }
                    else
                    {
                        Resource.activity.setActivityName(editTextActivity.getText().toString());
                        Resource.activity.setRate(editTextRate.getText().toString());
                        Resource.activity.setTime(selectTime);
                        updatepachage(Resource.activity);
                    }


                    //((MainActivity) getActivity()).register(editTextName.getText().toString(),editTextEmail.getText().toString(),editTextPassword.getText().toString());
                }


            }
        });

        ((MainActivity) getActivity()).textTitle.setText(R.string.add_activity);
        ((MainActivity) getActivity()).toolbar.setVisibility(View.VISIBLE);
        List<String> list = new ArrayList<String>();
        list.add("Hourly");
        list.add("Weely");
        list.add("Monthy");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        timeSpinner.setAdapter(dataAdapter);
            if(Resource.activity!=null) {
                editTextActivity.setText(Resource.activity.getActivityName());
                editTextRate.setText(Resource.activity.getRate());

            }
            else
            {
                Resource.activity=new Activities();

            }



        return view;


    }


    public String checkValidity(EditText activityname, EditText rate) {


        if (activityname.getText().toString().equals("")) {
            textinActivity.setErrorEnabled(true);
            textinActivity.setError("Insert Activity Name");
            editTextActivity.requestFocus();
            return "Please Provide a Email Address.";
        }
        if (activityname.getText().toString().equals(null)) {
            textinActivity.setErrorEnabled(true);
            textinActivity.setError("Insert Activity Name");
            editTextActivity.requestFocus();
            return "Please Provide a Email Address.";
        }

        if (rate.getText().toString().equals("")) {
            textinActivity.setErrorEnabled(true);
            textinActivity.setError("Insert Rate");
            editTextRate.requestFocus();
            return "Please Provide a Email Address.";
        }
        if (rate.getText().toString().equals(null)) {
            textinRate.setErrorEnabled(true);
            textinRate.setError("Insert Rate");
            editTextRate.requestFocus();
            return "Please Provide a Email Address.";
        }


        return "Success";
    }


    public void addpackage(final Activities activity)
    {

        dialog = BaseActivity.ShowConstantProgressNOTCAN(getActivity(), "",getResources().getString(R.string.processing));
        dialog.show();
        final RequestQueue queue = VolleySingleton.getInstance(getActivity()).getmRequestQueue();
        String url = (Resource.BASE_URL +"package/add.json");
        StringRequest stringRequest_add_package = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                dialog.dismiss();

                try {
                    JSONObject jsonObject = new JSONObject(response);  // response is a array come from API

                    boolean success = jsonObject.getBoolean("success");
                    if (success) {
                         JSONObject res=jsonObject.getJSONObject("response");
                        Resource.activity.setId(res.getInt("id"));
                        ((MainActivity) getActivity()).replaceFragment(new ActivityFragment());
                                           }
                         else {
                        JSONObject err = jsonObject.getJSONObject("error");
                        String errormessage = err.getString("description");
                        BaseActivity.showerror(getActivity(), errormessage);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }



            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                dialog.dismiss();
                //showerror("not set gcm id on server");

                Toast.makeText(getActivity(),R.string.package_not_added, Toast.LENGTH_SHORT).show();


            }

        }) {
            @Override
            public HashMap<String, String> getParams() {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("token",Resource.TOKEN);
                params.put("description",activity.getActivityName());
                params.put("price",activity.getRate());
                params.put("startingLatitude","0.0");
                params.put("startingLongitude","0.0");
                params.put("endingLatitude","0.0");
                params.put("endingLongitude","0.0");



                return params;
            }
        };

        stringRequest_add_package.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(stringRequest_add_package);

    }
    public void updatepachage(final Activities activity)
    {
        dialog = BaseActivity.ShowConstantProgressNOTCAN(getActivity(), "",getResources().getString(R.string.processing));
        dialog.show();
        final RequestQueue queue = VolleySingleton.getInstance(getActivity()).getmRequestQueue();
        String url = (Resource.BASE_URL +"package/update.json");
        StringRequest stringRequest_add_package = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                dialog.dismiss();

                try {
                    JSONObject jsonObject = new JSONObject(response);  // response is a array come from API

                    boolean success = jsonObject.getBoolean("success");
                    if (success) {
                        JSONObject res=jsonObject.getJSONObject("response");
                        Resource.activity.setId(res.getInt("id"));
                        ((MainActivity) getActivity()).replaceFragment(new ActivityFragment());
                    }
                    else {
                        JSONObject err = jsonObject.getJSONObject("error");
                        String message = err.getString("description");
                        BaseActivity.showerror(getActivity(),message);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


                //obj.register();

                // Toast.makeText(EWalletApplication.getContext(), " Register", Toast.LENGTH_SHORT).show();

            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                dialog.dismiss();
                //showerror("not set gcm id on server");

                Toast.makeText(getActivity(),R.string.package_not_updated, Toast.LENGTH_SHORT).show();


            }

        }) {
            @Override
            public HashMap<String, String> getParams() {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("token",Resource.TOKEN);
                params.put("description",activity.getActivityName());
                params.put("price",activity.getRate());
                params.put("package_id",String.valueOf(activity.getId()));
                params.put("startingLatitude","0.0");
                params.put("startingLongitude","0.0");
                params.put("endingLatitude","0.0");
                params.put("endingLongitude","0.0");



                return params;
            }
        };

        stringRequest_add_package.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(stringRequest_add_package);

    }
}
