package io.itmatic.tracktrot.Fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;

import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


import io.itmatic.tracktrot.MainActivity;
import io.itmatic.tracktrot.R;

public class SignUpFragment extends Fragment
{

    private TextView textRegister;
    private TextView textSignIn;

    private TextInputLayout textinName;
    private TextInputLayout textinPassword;
    private TextInputLayout textinRetypepassword;
    private TextInputLayout textinEmail;
    private EditText editTextName;
    private  EditText editTextPassword;
    private EditText editTextRepassword;
    private EditText editTextEmail;
    private Button btn_sign_up;
    private String regId;
    private ProgressDialog dialog;
    private TextView textFacebook;
    private TextView textGoogle;



    private SharedPreferences.Editor editor;

    public SignUpFragment() {
        // Required empty public constructor
    }

    public static SignUpFragment newInstance(String param1, String param2) {
        SignUpFragment fragment = new SignUpFragment();
        Bundle args = new Bundle();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        ((MainActivity) getActivity()).textTitle.setText(R.string.sign_up);
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_signup, container, false);
         textRegister=(TextView) view.findViewById(R.id.txt_register);
         textSignIn=(TextView) view.findViewById(R.id.txt_sign_in);

        ((MainActivity) getActivity()).textTitle.setText(R.string.sign_up);
        editor=getActivity().getSharedPreferences(getActivity().getPackageName(), Context.MODE_PRIVATE).edit();


        editTextName=(EditText) view.findViewById(R.id.edt_input_firstname);
        editTextPassword=(EditText) view.findViewById(R.id.edt_input_password);
        editTextRepassword=(EditText) view.findViewById(R.id.edt_input_repassword);
        editTextEmail=(EditText) view.findViewById(R.id.edt_input_email);
        textFacebook=(TextView) view.findViewById(R.id.txt_login_with_facebook);
        textGoogle=(TextView) view.findViewById(R.id.txt_login_with_google);
        textinName=(TextInputLayout) view.findViewById(R.id.input_layout_firstname);
        textinPassword=(TextInputLayout) view.findViewById(R.id.input_layout_password);
        textinRetypepassword=(TextInputLayout) view.findViewById(R.id.input_layout_repassword);
        textinEmail=(TextInputLayout) view.findViewById(R.id.input_layout_email);
        ((MainActivity) getActivity()).notification.setVisibility(View.GONE);
        ((MainActivity) getActivity()).list.setVisibility(View.GONE);
        editTextName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                textinName.setErrorEnabled(false);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        editTextEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                textinEmail.setErrorEnabled(false);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        editTextPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                textinPassword.setErrorEnabled(false);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        editTextRepassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                textinRetypepassword.setErrorEnabled(false);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });



        textFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                ((MainActivity) getActivity()).loginwithfacebook(false);

            }
        });

        textGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ((MainActivity) getActivity()).loginwithgoogle(false);


            }
        });




        textRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String result=checkValidity(editTextName,editTextEmail,editTextPassword,editTextRepassword);
                if(!result.equals("Success"))
                {
                    // lERROR.setVisibility(View.VISIBLE);

                }
                else
                {

                    ((MainActivity) getActivity()).register(editTextName.getText().toString(),editTextEmail.getText().toString(),editTextPassword.getText().toString());
                }



            }
        });

        textSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ((MainActivity) getActivity()).replaceFragment(new LoginFragment());
            }
        });
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {

    }



    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }



    public String checkValidity(EditText eusername,EditText eemail, EditText epassword, EditText erepassword) {

        if (eusername.getText().toString().equals("")) {
            textinName.setErrorEnabled(true);
            textinName.setError(getResources().getString(R.string.name_error));
            editTextName.requestFocus();
            return getResources().getString(R.string.name_error);
        }
        if (eusername.getText().toString().equals(null)) {
            textinName.setErrorEnabled(true);
            textinName.setError(getResources().getString(R.string.name_error));
            editTextName.requestFocus();
            return getResources().getString(R.string.name_error);
        }

        if (eemail.getText().toString().equals("")) {
            textinEmail.setErrorEnabled(true);
            textinEmail.setError(getResources().getString(R.string.email_error));
            editTextEmail.requestFocus();
            return getResources().getString(R.string.email_error);
        }
        if (eemail.getText().toString().equals(null)) {
            textinEmail.setErrorEnabled(true);
            textinEmail.setError(getResources().getString(R.string.email_error));
            editTextEmail.requestFocus();
            return getResources().getString(R.string.email_error);
        }
        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(eemail.getText().toString()).matches()) {
            textinEmail.setErrorEnabled(true);
            textinEmail.setError(getResources().getString(R.string.invalide_email_error));
            editTextEmail.requestFocus();
            return getResources().getString(R.string.invalide_email_error);
        }

        if (epassword.getText().toString().equals(null)) {
            textinPassword.setErrorEnabled(true);
            textinPassword.setError(getResources().getString(R.string.password_error));
            editTextPassword.requestFocus();
            return getResources().getString(R.string.password_error);
        }
        if (epassword.getText().toString().equals("")) {
            textinPassword.setErrorEnabled(true);
            textinPassword.setError(getResources().getString(R.string.password_error));
            editTextPassword.requestFocus();
            return getResources().getString(R.string.password_error);
        }

        if (erepassword.getText().toString().equals(null)) {
            textinRetypepassword.setErrorEnabled(true);
            textinRetypepassword.setError(getResources().getString(R.string.retype_password_error));
            editTextRepassword.requestFocus();
            return getResources().getString(R.string.retype_password_error);
        }
        if (erepassword.getText().toString().equals("")) {
            textinRetypepassword.setErrorEnabled(true);
            textinRetypepassword.setError(getResources().getString(R.string.retype_password_error));
            editTextRepassword.requestFocus();
            return getResources().getString(R.string.retype_password_error);
        }
        if(!erepassword.getText().toString().equals(epassword.getText().toString())) {
            textinRetypepassword.setErrorEnabled(true);
            textinRetypepassword.setError(getResources().getString(R.string.password_not_match_error));
            editTextRepassword.requestFocus();
            return getResources().getString(R.string.password_not_match_error);

        }



        return "Success";
    }









}
