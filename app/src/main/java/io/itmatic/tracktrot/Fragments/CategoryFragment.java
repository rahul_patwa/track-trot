package io.itmatic.tracktrot.Fragments;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import io.itmatic.tracktrot.Adapters.SetCategoryAdapter;
import io.itmatic.tracktrot.CommonClass.BaseActivity;
import io.itmatic.tracktrot.CommonClass.Resource;
import io.itmatic.tracktrot.MainActivity;
import io.itmatic.tracktrot.Model.Category;
import io.itmatic.tracktrot.R;
import io.itmatic.tracktrot.volley.VolleySingleton;

/**
 * A simple {@link Fragment} subclass.
 */
public class CategoryFragment extends Fragment {

    private SetCategoryAdapter adapter;
    private ProgressDialog dialog;
    private StringRequest LastSignRequest = null;
    private RecyclerView recyclerView;
    private TextView save;
    private TextView cancel;

    private ImageLoader mIMAGELOADER;

    public CategoryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_category, container, false);
        save = (TextView) view.findViewById(R.id.txt_save);
        cancel = (TextView) view.findViewById(R.id.txt_cancel);
        ((MainActivity) getActivity()).textTitle.setText(R.string.select_category);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycle_view);
        ((MainActivity) getActivity()).notification.setVisibility(View.GONE);
        ((MainActivity) getActivity()).list.setVisibility(View.GONE);
        mIMAGELOADER = VolleySingleton.getInstance(getActivity()).getImageLoader();
        getcategory(Resource.TOKEN);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Resource.csvList = "";
                Resource.usercategories.clear();
                for (int i = 0; i < Resource.categories.size(); i++) {
                    if (Resource.categories.get(i).getSelected_status() == 1) {
                        Resource.csvList = Resource.csvList + Resource.categories.get(i).getId() + ",";
                        Resource.usercategories.add(Resource.categories.get(i));

                    }
                }

                if (Resource.WHO == "guide") {
                    ((MainActivity) getActivity()).replaceFragment(new ActivityFragment());
                } else {

                    ((MainActivity) getActivity()).replaceFragment(new MapViewFragment());

                }
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ((MainActivity) getActivity()).replaceFragment(new ActivityFragment());


            }
        });

        return view;
    }


    public void getcategory(final String token) {

        dialog = BaseActivity.ShowConstantProgressNOTCAN(getActivity(), "", getResources().getString(R.string.processing));
        dialog.show();

        final RequestQueue queue = VolleySingleton.getInstance(getActivity()).getmRequestQueue();


        String url = (Resource.BASE_URL + "category/all.json?token=" + token);

        StringRequest getcat = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                dialog.dismiss();
                try {

                    JSONObject jsonObject = new JSONObject(response);  // response is a array come from API

                    boolean success = jsonObject.getBoolean("success");
                    if (success) {

                        JSONArray data = jsonObject.getJSONArray("response");

                        Resource.categories.clear();
                        for (int i = 0; i < data.length(); i++) {
                            Category category = new Category();
                            JSONObject cat = data.getJSONObject(i);
                            category.setId(cat.getInt("id"));
                            category.setName(cat.getString("name"));
                            category.setEnabled(cat.getBoolean("enabled"));
                            category.setUrl(cat.getString("image_url"));

                            Resource.categories.add(category);


                        }

                        adapter = new SetCategoryAdapter(Resource.categories, CategoryFragment.this, mIMAGELOADER);
                        GridLayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 2);
                        mLayoutManager.getPaddingLeft();
                        recyclerView.setLayoutManager(mLayoutManager);
                        recyclerView.setAdapter(adapter);


                    } else {
                        String error = jsonObject.getString("error");
                        BaseActivity.showerror(getActivity(), error);

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


                //Toast.makeText(EWalletApplication.getContext(), "login", Toast.LENGTH_SHORT).show();

            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError err) {
                dialog.dismiss();

                Toast.makeText(getActivity(), R.string.category_not_getting, Toast.LENGTH_SHORT).show();


            }

        }) {
            @Override
            public HashMap<String, String> getParams() {
                HashMap<String, String> params = new HashMap<String, String>();

                return params;
            }


        };

        getcat.setRetryPolicy(new DefaultRetryPolicy(10000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        if (LastSignRequest != null && !LastSignRequest.hasHadResponseDelivered()) {
            LastSignRequest.cancel();
        }

        LastSignRequest = getcat;
        getcat.setShouldCache(false);
        queue.add(getcat);

    }

}
