package io.itmatic.tracktrot.Fragments;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.facebook.CallbackManager;

import io.itmatic.tracktrot.MainActivity;
import io.itmatic.tracktrot.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends Fragment {

    private TextView textSignup;
    private TextView textSignin;
    private SharedPreferences.Editor prefEditor;
    private TextView textFacebook;
    private TextView textGoogle;

    private TextInputLayout textinPassword;

    private TextInputLayout textinEmail;

    private EditText editTextPassword;

    private EditText editTextEmail;

    public LoginFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        ((MainActivity) getActivity()).textTitle.setText(R.string.sign_in);
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_login, container, false);

        textSignup = (TextView) view.findViewById(R.id.txt_sign_up);
        textSignin = (TextView) view.findViewById(R.id.txt_login);
        textFacebook = (TextView) view.findViewById(R.id.txt_login_with_facebook);
        textGoogle = (TextView) view.findViewById(R.id.txt_login_with_google);
        prefEditor = getActivity().getSharedPreferences(getActivity().getPackageName(), Context.MODE_PRIVATE).edit();
        editTextPassword = (EditText) view.findViewById(R.id.edt_input_password);
        editTextEmail = (EditText) view.findViewById(R.id.edt_input_email);
        textinPassword = (TextInputLayout) view.findViewById(R.id.input_layout_password);
        textinEmail = (TextInputLayout) view.findViewById(R.id.input_layout_email);
        ((MainActivity) getActivity()).notification.setVisibility(View.GONE);
        ((MainActivity) getActivity()).list.setVisibility(View.GONE);

        // after showing the error. it will hide when edit text
        editTextEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                textinEmail.setErrorEnabled(false);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        editTextPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                textinPassword.setErrorEnabled(false);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        // set the title of toolbar
        ((MainActivity) getActivity()).textTitle.setText(R.string.sign_in);

        //switch login fragment to sign up in that case when account not exist
        textSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ((MainActivity) getActivity()).replaceFragment(new SignUpFragment());

            }
        });

        //call sign in

        textSignin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //check validation of all fields according their perspectives
                String result = checkValidity(editTextEmail, editTextPassword);
                if (!result.equals("Success")) {
                    // lERROR.setVisibility(View.VISIBLE);

                } else {
                    //call login method
                    ((MainActivity) getActivity()).login(editTextEmail.getText().toString(), editTextPassword.getText().toString());
                }
            }
        });


        //login by facebook
        textFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                ((MainActivity) getActivity()).loginwithfacebook(true);

            }
        });

        //login by google plus
        textGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).loginwithgoogle(true);

            }
        });


        return view;
    }




    //method for set validation for all fields
    public String checkValidity(EditText eemail, EditText epassword) {


        if (eemail.getText().toString().equals("")) {
            textinEmail.setErrorEnabled(true);
            textinEmail.setError(getResources().getString(R.string.email_error));
            editTextEmail.requestFocus();
            return getResources().getString(R.string.email_error);
        }
        if (eemail.getText().toString().equals(null)) {
            textinEmail.setErrorEnabled(true);
            textinEmail.setError(getResources().getString(R.string.email_error));
            editTextEmail.requestFocus();
            return getResources().getString(R.string.email_error);
        }
        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(eemail.getText().toString()).matches()) {
            textinEmail.setErrorEnabled(true);
            textinEmail.setError(getResources().getString(R.string.invalide_email_error));
            editTextEmail.requestFocus();
            return getResources().getString(R.string.invalide_email_error);
        }

        if (epassword.getText().toString().equals(null)) {
            textinPassword.setErrorEnabled(true);
            textinPassword.setError(getResources().getString(R.string.password_error));
            editTextPassword.requestFocus();
            return getResources().getString(R.string.password_error);
        }
        if (epassword.getText().toString().equals("")) {
            textinPassword.setErrorEnabled(true);
            textinPassword.setError(getResources().getString(R.string.password_error));
            return getResources().getString(R.string.password_error);
        }


        return "Success";
    }


}
