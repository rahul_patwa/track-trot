package io.itmatic.tracktrot.Fragments;


import android.Manifest;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.isseiaoki.simplecropview.CropImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;

import io.itmatic.tracktrot.CommonClass.BaseActivity;
import io.itmatic.tracktrot.CommonClass.MultiPartRequest;
import io.itmatic.tracktrot.CommonClass.Resource;
import io.itmatic.tracktrot.MainActivity;
import io.itmatic.tracktrot.R;
import io.itmatic.tracktrot.volley.VolleySingleton;

/**
 * A simple {@link Fragment} subclass.
 */
public class UploadGuideImages extends Fragment {

    private TextView textUpload;
    public ImageView imgUser;
    public ImageView idProof1;
    public ImageView idProof2;
    private CropImageView cropImageView;
    private TextView textOk;
    private File passPort = null;
    private File licence = null;
    private File profile = null;
    private Bitmap croppedBitmap;
    private Dialog dialog;
    private Intent intent;
    private final String[] GELLARY_PERMS = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    private final String[] CAMERA_PERMS = {Manifest.permission.CAMERA};
    private final int CAMERA_REQUEST = 1337;
    private final int GELLARY_REQUEST = 1340;
    private StringRequest LastSignRequest = null;

    public UploadGuideImages() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_upload_guide_images, container, false);


        textUpload = (TextView) view.findViewById(R.id.upload);
        imgUser = (ImageView) view.findViewById(R.id.img_user);
        idProof1 = (ImageView) view.findViewById(R.id.img_idproof1);
        idProof2 = (ImageView) view.findViewById(R.id.img_idproof2);
        textOk = (TextView) view.findViewById(R.id.txt_ok);
        cropImageView = (CropImageView) view.findViewById(R.id.cropImageView);
        cropImageView.setCropMode(CropImageView.CropMode.CIRCLE);
        cropImageView.setVisibility(View.GONE);
        textOk.setVisibility(View.GONE);
        ((MainActivity) getActivity()).notification.setVisibility(View.GONE);
        ((MainActivity) getActivity()).list.setVisibility(View.GONE);
        ((MainActivity) getActivity()).textTitle.setText(R.string.upload_image);
        textUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (profile != null && passPort != null && licence != null) {
                    uploadimage(profile, passPort, licence);
                } else {
                    Toast.makeText(getActivity(), "Please Select Images", Toast.LENGTH_LONG).show();
                }

            }
        });

        imgUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Resource.IMAGETYPE = 1;
                setimage();

            }
        });

        idProof1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Resource.IMAGETYPE = 2;
                setimage();
            }
        });

        idProof2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Resource.IMAGETYPE = 3;
                setimage();

            }
        });


        textOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                croppedBitmap = (cropImageView.getCroppedBitmap());
                imgUser.setImageBitmap(croppedBitmap);
                cropImageView.setVisibility(View.GONE);
                textOk.setVisibility(View.GONE);
                imgUser.setVisibility(View.VISIBLE);
                textUpload.setVisibility(View.VISIBLE);
                profile = new File(getActivity().getCacheDir(), "userfile");
                try {
                    profile.createNewFile();


//Convert bitmap to byte array

                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    croppedBitmap.compress(Bitmap.CompressFormat.JPEG, 0 /*ignored for PNG*/, bos);
                    byte[] bitmapdata = bos.toByteArray();

//write the bytes in file
                    FileOutputStream fos = new FileOutputStream(profile);
                    fos.write(bitmapdata);
                    fos.flush();
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });


        return view;
    }


    public void getImage(Bitmap bitmap) {

        textOk.setVisibility(View.VISIBLE);
        textUpload.setVisibility(View.GONE);
        cropImageView.setVisibility(View.VISIBLE);
        imgUser.setVisibility(View.GONE);
        cropImageView.setImageBitmap(bitmap);
    }

    public void setimageproof1(Bitmap proof1) {
        idProof1.setImageBitmap(proof1);
        passPort = new File(getActivity().getCacheDir(), "passport");
        try {
            passPort.createNewFile();


//Convert bitmap to byte array

            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            proof1.compress(Bitmap.CompressFormat.JPEG, 0 /*ignored for PNG*/, bos);
            byte[] bitmapdata = bos.toByteArray();

//write the bytes in file
            FileOutputStream fos = new FileOutputStream(passPort);
            fos.write(bitmapdata);
            fos.flush();
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setimageproof2(Bitmap proof2) {
        idProof2.setImageBitmap(proof2);
        licence = new File(getActivity().getCacheDir(), "userfile");
        try {
            licence.createNewFile();


//Convert bitmap to byte array

            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            proof2.compress(Bitmap.CompressFormat.JPEG, 0 /*ignored for PNG*/, bos);
            byte[] bitmapdata = bos.toByteArray();

//write the bytes in file
            FileOutputStream fos = new FileOutputStream(licence);
            fos.write(bitmapdata);
            fos.flush();
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void uploadimage(final File file, final File pass, final File lice) {

        dialog = BaseActivity.ShowConstantProgressNOTCAN(getActivity(), "", "Uploading Images....");
        dialog.show();

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("token", Resource.TOKEN);
        HashMap<String, File> param = new HashMap<String, File>();
        param.put("file", file);
        param.put("passportImage", pass);
        param.put("drivingLicenceImage", lice);

        final RequestQueue queue = VolleySingleton.getInstance(getActivity()).getmRequestQueue();


        String url = (Resource.BASE_URL + "user/update/avatar.json");

        MultiPartRequest stringReques_upload_image = new MultiPartRequest(url, file, params, param, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                dialog.dismiss();

                try {
                    JSONObject data = new JSONObject(response);  // response is a array come from API
                    boolean status = data.getBoolean("success");// success is a status come from API
                    if (status == true) {
                        JSONObject usereditdetail = data.getJSONObject("response");
                        String token = usereditdetail.getString("access_token");
                        //if(Resource.WHO.equals("guide"))
                        ((MainActivity) getActivity()).replaceFragment(new CategoryFragment());


                    } else {

                        JSONObject userdetaillist = data.getJSONObject("error");
                        String error = userdetaillist.getString("description");
                        BaseActivity.showerror(getActivity(), error);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                error.getStackTrace();
                // Toast.makeText(io.itmatic.mavenika.EditProfile.this, "no Internet connection", Toast.LENGTH_LONG).show();

            }

        }) {
            @Override
            public HashMap<String, String> getParams() {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("token", Resource.TOKEN);
                return params;
            }


        };

        stringReques_upload_image.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        stringReques_upload_image.setShouldCache(false);
        queue.add(stringReques_upload_image);


    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {


        switch (requestCode)


        {
            case CAMERA_REQUEST:
                if (canAccessCamera()) {

                    intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, 7);
                }
            case GELLARY_REQUEST:
                if (canAccessGellary()) {
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
                    startActivityForResult(Intent.createChooser(intent, "Open Gallery"), 6);
                }


        }
    }

    public void setimage() {
        AlertDialog.Builder getImageFrom = new AlertDialog.Builder(getActivity());
        getImageFrom.setTitle("Select Image");
        final CharSequence[] opsChars = {"Take Picture", "Open Gallery"};
        getImageFrom.setItems(opsChars, new android.content.DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == 0) {

                    //outputFileUri = Uri.fromFile(file);
                    String file = System.currentTimeMillis() + ".jpg";
                    File newfile = new File(file);
                    try {
                        newfile.createNewFile();
                    } catch (IOException e) {
                    }

                    Uri outputFileUri = Uri.fromFile(newfile);
                    if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) ==
                            PackageManager.PERMISSION_GRANTED) {
                        intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(intent, 7);
                    } else {
                        ActivityCompat.requestPermissions(getActivity(), CAMERA_PERMS, CAMERA_REQUEST);
                    }

                } else if (which == 1) {

                    if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) ==
                            PackageManager.PERMISSION_GRANTED) {
                        Intent intent = new Intent();
                        intent.setType("image/*");
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
                        startActivityForResult(Intent.createChooser(intent, "Open Gallery"), 6);
                    } else {
                        ActivityCompat.requestPermissions(getActivity(), GELLARY_PERMS, GELLARY_REQUEST);
                    }
                }

            }
        });

        getImageFrom.show();

    }

    private boolean canAccessCamera() {
        return (hasPermission(Manifest.permission.CAMERA));
    }

    private boolean canAccessGellary() {
        return (hasPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE));
    }

    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ContextCompat.checkSelfPermission(getActivity(), perm));
    }
}
