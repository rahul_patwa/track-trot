package io.itmatic.tracktrot.Fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;


import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.mukesh.countrypicker.fragments.CountryPicker;
import com.mukesh.countrypicker.interfaces.CountryPickerListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import io.itmatic.tracktrot.CommonClass.BaseActivity;
import io.itmatic.tracktrot.CommonClass.Resource;
import io.itmatic.tracktrot.MainActivity;
import io.itmatic.tracktrot.R;
import io.itmatic.tracktrot.volley.VolleySingleton;


public class VarifyMobileFragment extends Fragment {


    private TextView textSend;
    private EditText editTextMobile;
    private TextInputLayout textinMobile;
    private EditText editTextCode;
    private TextInputLayout textinCode;
    private StringRequest LastSignRequest = null;
    private ProgressDialog dialog;
    private  CountryPicker picker;
    private String dcoded;
     private String coded;
    public VarifyMobileFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_varify_mobile, container, false);
        textSend=(TextView) view.findViewById(R.id.txt_send_varification_code_btn);
        editTextMobile=(EditText) view.findViewById(R.id.edt_mobile);
        textinMobile=(TextInputLayout) view.findViewById(R.id.input_layout_mobile);
        editTextCode=(EditText) view.findViewById(R.id.edt_code);
        textinCode=(TextInputLayout) view.findViewById(R.id.input_layout_code);
        ((MainActivity) getActivity()).textTitle.setText(R.string.VerifyMobile);
        ((MainActivity) getActivity()).notification.setVisibility(View.GONE);
        ((MainActivity) getActivity()).list.setVisibility(View.GONE);

        picker = CountryPicker.newInstance("Select Country");
        textSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String result=checkValidity(editTextMobile);
                if(!result.equals("Success"))
                {
                    // lERROR.setVisibility(View.VISIBLE);

                }
                else
                {

                    dcoded=editTextCode.getText().toString().replaceAll("\\D+","");
                    coded=dcoded+editTextMobile.getText().toString();

                    varifyMobile(coded);
                }
            }
        });

        editTextCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                picker.show(getFragmentManager(), "COUNTRY_PICKER");

            }
        });

        picker.setListener(new CountryPickerListener() {
            @Override
            public void onSelectCountry(String name, String code, String dialCode, int flagDrawableResID) {
                dcoded=dialCode.replaceAll(" + ","");;
                editTextCode.setText(dialCode);
                picker.dismiss();
               // fullnumberwithcode=dcode+editTextMobile.toString();

            }
        });
        return view;
    }





    public String checkValidity(EditText mobile) {


        if (mobile.getText().toString().equals("")) {
            textinMobile.setErrorEnabled(true);
            textinMobile.setError("Please Provide Mobile No.");
            editTextMobile.requestFocus();
            return "Please Provide a Email Address.";
        }
        if (mobile.getText().toString().equals(null)) {
            textinMobile.setErrorEnabled(true);
            textinMobile.setError("Please Provide Mobile No.");
            editTextMobile.requestFocus();
            return "Please Provide a Email Address.";
        }
        if (mobile.getText().toString().length()!=10) {
            textinMobile.setErrorEnabled(true);
            textinMobile.setError("Please Provide a Email Address");
            editTextMobile.requestFocus();
            return "Note Valid Email Address";
        }
        return "Success";

    }


    private void varifyMobile(final String mobile) {

        dialog = BaseActivity.ShowConstantProgressNOTCAN(getActivity(), "",getResources().getString(R.string.verifying_number));
        dialog.show();

        final RequestQueue queue = VolleySingleton.getInstance(getActivity()).getmRequestQueue();


        String url = (Resource.BASE_URL + "user/otp/send.json");

        StringRequest stringRequest_sign_up = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                dialog.dismiss();
                try {

                    JSONObject jsonObject = new JSONObject(response);  // response is a array come from API

                    boolean success = jsonObject.getBoolean("success");
                    if (success) {

                        Resource.MOBILE=mobile;
                        ((MainActivity) getActivity()).replaceFragment(new EnterOtpFragment());




                    } else {
                        JSONObject err = jsonObject.getJSONObject("error");
                        String errormessage = err.getString("description");
                        BaseActivity.showerror(getActivity(), errormessage);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


                //Toast.makeText(EWalletApplication.getContext(), "login", Toast.LENGTH_SHORT).show();

            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError err) {
                dialog.dismiss();

                BaseActivity.showerror(getActivity(), "Server Error");

                // Toast.makeText(EWalletApplication.getContext(), " Not Register", Toast.LENGTH_SHORT).show();


            }

        }) {
            @Override
            public HashMap<String, String> getParams() {
                HashMap<String, String> params = new HashMap<String, String>();

                params.put("phone",mobile);
                params.put("token",Resource.TOKEN);

                return params;
            }


        };

        stringRequest_sign_up.setRetryPolicy(new DefaultRetryPolicy(10000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        if (LastSignRequest != null && !LastSignRequest.hasHadResponseDelivered()) {
            LastSignRequest.cancel();
        }

        LastSignRequest = stringRequest_sign_up;
        stringRequest_sign_up.setShouldCache(false);
        queue.add(stringRequest_sign_up);
    }



    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }



}
