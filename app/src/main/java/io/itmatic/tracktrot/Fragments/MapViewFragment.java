package io.itmatic.tracktrot.Fragments;


import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.itmatic.tracktrot.CommonClass.BaseActivity;
import io.itmatic.tracktrot.CommonClass.Resource;
import io.itmatic.tracktrot.MainActivity;
import io.itmatic.tracktrot.Model.Guide;
import io.itmatic.tracktrot.R;
import io.itmatic.tracktrot.volley.VolleySingleton;

/**
 * A simple {@link Fragment} subclass.
 */
public class MapViewFragment extends Fragment implements OnMapReadyCallback {


    private boolean isGPSEnabled = false;
    private boolean isNetWorkEnabled = false;
    private boolean IsLocationAvailable = false;
    private TextView nADDRESS;
    private List<Address> addresses = new ArrayList<>();
    private FindLocation_Adapter mAdapter;
    ListView listview;
    private LocationManager locationManager;
    private final String[] sINITIAL_PERMS = {
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
    };
    private double dLAT = 0;
    private double dLONG = 0;
    private RelativeLayout nSEARCH;
    private StringRequest LastSignRequest = null;
    private static final long lMINTIME = 0;
    private static final float fMINDISTANCE = 0;
    private final int iINITIAL_REQUEST = 1337;
    private ProgressDialog dialog;
    private GoogleMap googleMap;
    private MapView mapView;


    public MapViewFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_map_view, container, false);
        //showdrawer method for showing the drawer with this fragment
        ((MainActivity) getActivity()).showdrawer();
        //to change the toolbar title
        nADDRESS = (TextView) view.findViewById(R.id.txt_address);
        listview = (ListView) view.findViewById(R.id.listview);
        mapView = (MapView) view.findViewById(R.id.map);
        mapView.onResume();
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);
        nSEARCH = (RelativeLayout) view.findViewById(R.id.lout_search);
        ((MainActivity) getActivity()).textTitle.setText(R.string.app_name);
        ((MainActivity) getActivity()).toolbar.setBackgroundColor(getResources().getColor(R.color.white));
        if (Resource.WHO == "guide") {
            ((MainActivity) getActivity()).notification.setVisibility(View.VISIBLE);
            ((MainActivity) getActivity()).list.setVisibility(View.GONE);
        } else {
            ((MainActivity) getActivity()).notification.setVisibility(View.GONE);
            ((MainActivity) getActivity()).list.setVisibility(View.VISIBLE);
        }

        nSEARCH.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                //((MainActivity) getActivity()).replaceFragment(new FindLocationFragment());

            }
        });

        nADDRESS.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {


                if (nADDRESS.getText().toString() != "" && !nADDRESS.getText().toString().equals("") && nADDRESS.getText().toString() != null && !nADDRESS.getText().toString().equals(null)) {
                    String address = nADDRESS.getText().toString();
                    Geocoder geocoder = new Geocoder(getActivity());
                    listview.setVisibility(View.VISIBLE);
                    try {
                        addresses = geocoder.getFromLocationName(address, 15);

                        mAdapter = new FindLocation_Adapter(getActivity(), R.layout.locationlist, addresses);

                        listview.setAdapter(mAdapter);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    listview.setVisibility(View.GONE);
                }

            }
        });
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Address address = addresses.get(position);

                LatLng latLng = new LatLng(address.getLatitude(), address.getLongitude());
                // Show the current location in Google Map
                googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                googleMap.animateCamera(CameraUpdateFactory.zoomTo(12));
                getguidesbycategory(Resource.csvList, Resource.TOKEN, address.getLatitude(), address.getLongitude());

                listview.setVisibility(View.GONE);

            }


        });

        if (ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) ==
                        PackageManager.PERMISSION_GRANTED) {
            try {
                //initilizeMap method for initilize googleMap object
                initilizeMap();
            } catch (Exception e) {
                e.printStackTrace();
            }
            googleMap.setMyLocationEnabled(true);
            googleMap.animateCamera(CameraUpdateFactory.zoomTo(15));

            googleMap.getUiSettings().setZoomGesturesEnabled(true);
            googleMap.getUiSettings().setRotateGesturesEnabled(true);

            getLocation();
        } else {
            requestPermissions(sINITIAL_PERMS, iINITIAL_REQUEST);
        }


        return view;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {


        switch (requestCode)


        {
            case iINITIAL_REQUEST:
                if (canAccessFineLocation() || canAccessCoarseLocation()) {

                    try {
                        initilizeMap();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    googleMap.setMyLocationEnabled(true);
                    googleMap.animateCamera(CameraUpdateFactory.zoomTo(12));

                    googleMap.getUiSettings().setZoomGesturesEnabled(true);
                    googleMap.getUiSettings().setRotateGesturesEnabled(true);
                    getLocation();
                }


        }


    }


    private void initilizeMap() {


        View locationButton = ((View) mapView.findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));

        RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();
        // position on right bottom
        rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
        rlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
        rlp.setMargins(0, 0, 30, 30);

        // check if map is created successfully or not
        if (googleMap == null) {
            Toast.makeText(getActivity(), "Sorry! unable to create maps", Toast.LENGTH_SHORT)
                    .show();

        }

    }


    private boolean canAccessFineLocation() {
        return (hasPermission(Manifest.permission.ACCESS_FINE_LOCATION));
    }

    private boolean canAccessCoarseLocation() {
        return (hasPermission(Manifest.permission.ACCESS_COARSE_LOCATION));
    }


    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ContextCompat.checkSelfPermission(getActivity(), perm));
    }


    public void getLocation() {
        locationManager = (LocationManager) getActivity().getSystemService(getActivity().LOCATION_SERVICE);
        isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        isNetWorkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        if (isGPSEnabled || isNetWorkEnabled) {


            IsLocationAvailable = true;
            if (isNetWorkEnabled) {
                getLocationIfAvailable(IsLocationAvailable, 1);
            } else {
                getLocationIfAvailable(IsLocationAvailable, 2);
            }
        }
    }

    /*********************************************************************************************************************************/

    public void getLocationIfAvailable(Boolean CanGetLocation, int LocationProvider) {


        switch (LocationProvider) {
            case 0:

                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    ActivityCompat.requestPermissions(getActivity(), sINITIAL_PERMS, iINITIAL_REQUEST);
                }
                locationManager.requestLocationUpdates(LocationManager.PASSIVE_PROVIDER, lMINTIME, fMINDISTANCE, listener);
                break;
            case 1:
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, lMINTIME, fMINDISTANCE, listener);
                break;
            case 2:
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, lMINTIME, fMINDISTANCE, listener);
                break;
            default:
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, lMINTIME, fMINDISTANCE, listener);
                break;
        }
    }

    @Override
    public void onMapReady(GoogleMap Map) {


        googleMap = Map;

    }


    public class FindLocation_Adapter extends ArrayAdapter<Address> {

        // declaring our ArrayList of items
        private List<Address> objects;


        public FindLocation_Adapter(Context context, int textViewResourceId, List<Address> objects) {
            super(context, textViewResourceId, objects);
            this.objects = objects;
        }


        public View getView(int position, View convertView, ViewGroup parent) {


            View v = convertView;


            if (v == null) {
                LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = inflater.inflate(R.layout.locationlist, null);
            }


            final Address ob = objects.get(position);

            if (ob != null) {

               /* final TextView rBANK=(TextView) v.findViewById(R.id.txt_bank);
                RelativeLayout rBANKLISTITEM=(RelativeLayout) v.findViewById(R.id.routbanklistitem);

                rBANK.setText(ob.getBank_name());*/
                TextView locationname = (TextView) v.findViewById(R.id.location_name);
                TextView locationaddress = (TextView) v.findViewById(R.id.location_address);

                locationname.setText(ob.getAddressLine(0) + "," + ob.getAddressLine(2));
                locationaddress.setText(ob.getAdminArea() + "," + ob.getCountryName());


            }


            return v;

        }


    }

    /************************************************************************************************************************************/

    //Listener for getting the current location
    LocationListener listener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {


            if (dLAT == 0 && dLONG == 0) {

                dLAT = location.getLatitude();
                dLONG = location.getLongitude();

                LatLng latLng = new LatLng(dLAT, dLONG);
                // Show the current location in Google Map
                googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                //  updatelocation(nLAT, nLONG, token);
                if (Resource.WHO == "guide") {
                    // ((MainActivity) getActivity()).replaceFragment(new ActivityFragment());
                } else {

                    getguidesbycategory(Resource.csvList, Resource.TOKEN, location.getLatitude(), location.getLongitude());

                }

            } else {
                setUpMap(location.getLatitude(), location.getLongitude());
            }


        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {
            Toast.makeText(getActivity(), provider, Toast.LENGTH_SHORT).show();
        }
    };


    public void setUpMap(Double lat, Double lng) {


        //set map type
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        // Get latitude of the current location

        // Create a LatLng object for the current location
        LatLng latLng = new LatLng(lat, lng);
        // Show the current location in Google Map
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(12));
        // Zoom in the Google Map
    }


    public void getguidesbycategory(final String csvList, final String token, final double lat, final double lng) {
        dialog = BaseActivity.ShowConstantProgressNOTCAN(getActivity(), "", getResources().getString(R.string.processing));
        dialog.show();

        final RequestQueue queue = VolleySingleton.getInstance(getActivity()).getmRequestQueue();


        String url = (Resource.BASE_URL + "travellers/get/nearby/guides.json?token=" + token + "&categories=" + csvList + "&longitude=" + lng + "&latitude=" + lat);

        StringRequest getcat = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                dialog.dismiss();
                try {

                    JSONObject jsonObject = new JSONObject(response);  // response is a array come from API

                    boolean success = jsonObject.getBoolean("success");
                    if (success) {

                        JSONArray data = jsonObject.getJSONArray("response");

                        Resource.guides.clear();
                        for (int i = 0; i < data.length(); i++) {
                            Guide guide = new Guide();
                            JSONObject gud = data.getJSONObject(i);
                            guide.setId(gud.getInt("id"));
                            guide.setName(gud.getString("first_name"));
                            guide.setPhone(gud.getString("phone"));
                            guide.setToken(gud.getString("access_token"));
                            JSONObject profile = gud.getJSONObject("profile");
                            guide.setLat(profile.getDouble("latitude"));
                            guide.setLng(profile.getDouble("longitude"));
                            MarkerOptions marker = new MarkerOptions().position(new LatLng(profile.getDouble("latitude"), profile.getDouble("longitude"))).title("Guide");
                            marker.icon(BitmapDescriptorFactory.defaultMarker());
                            googleMap.addMarker(marker);
                            guide.setPrice(profile.getInt("price"));
                            guide.setUrlIdProof(profile.getString("passoport_image_url"));
                            guide.setUrlImage(profile.getString("dl_image_url"));
                            guide.setAverage_rating(profile.getInt("average_rating"));


                            Resource.guides.add(guide);


                        }

                        // ((MainActivity) getActivity()).replaceFragment(new MapViewFragment());


                    } else {
                        String error = jsonObject.getString("error");
                        BaseActivity.showerror(getActivity(), error);

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


                //Toast.makeText(EWalletApplication.getContext(), "login", Toast.LENGTH_SHORT).show();

            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError err) {
                dialog.dismiss();

                // Toast.makeText(EWalletApplication.getContext(), " Not Register", Toast.LENGTH_SHORT).show();


            }

        }) {
            @Override
            public HashMap<String, String> getParams() {
                HashMap<String, String> params = new HashMap<String, String>();

                return params;
            }


        };

        getcat.setRetryPolicy(new DefaultRetryPolicy(10000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        if (LastSignRequest != null && !LastSignRequest.hasHadResponseDelivered()) {
            LastSignRequest.cancel();
        }

        LastSignRequest = getcat;
        getcat.setShouldCache(false);
        queue.add(getcat);

    }


}
