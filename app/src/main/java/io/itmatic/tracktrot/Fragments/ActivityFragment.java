package io.itmatic.tracktrot.Fragments;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import io.itmatic.tracktrot.Adapters.ActivityAdapter;
import io.itmatic.tracktrot.Adapters.CategoryAdapter;
import io.itmatic.tracktrot.CommonClass.BaseActivity;
import io.itmatic.tracktrot.CommonClass.Resource;
import io.itmatic.tracktrot.MainActivity;
import io.itmatic.tracktrot.Model.Activities;
import io.itmatic.tracktrot.R;
import io.itmatic.tracktrot.volley.VolleySingleton;

/**
 * A simple {@link Fragment} subclass.
 */
public class ActivityFragment extends Fragment {

    private ImageView imgAdd;
    private ImageView imgRemove;

    private CategoryAdapter adapter;
    private ActivityAdapter activityAdapter;
    private RecyclerView recyclerView;
    private RecyclerView activityRecycleView;
    /*  private EditText editTextActivity;
      private EditText editTextTime;
      private EditText editTextRate;*/
    private ImageView imgAddActivity;

    private Activities selOb;
    private EditText guidePrice;
    private ProgressDialog dialog;
    private TextView save;

    public ActivityFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_activity, container, false);

        imgAdd = (ImageView) view.findViewById(R.id.img_add);
        imgRemove = (ImageView) view.findViewById(R.id.img_remove);
       /* editTextActivity = (EditText) view.findViewById(R.id.edt_activity);
        editTextTime = (EditText) view.findViewById(R.id.edt_time);
        editTextRate = (EditText) view.findViewById(R.id.edt_rate);*/
        imgAddActivity = (ImageView) view.findViewById(R.id.img_add_activity);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycle_view);
        save = (TextView) view.findViewById(R.id.txt_save);
        guidePrice = (EditText) view.findViewById(R.id.edt_getrate);
        activityRecycleView = (RecyclerView) view.findViewById(R.id.activity_recycle_view);
        //registerForContextMenu(activityRecycleView);
        ((MainActivity) getActivity()).list.setVisibility(View.GONE);
        ((MainActivity) getActivity()).notification.setVisibility(View.GONE);
        ((MainActivity) getActivity()).toolbar.setVisibility(View.GONE);


        adapter = new CategoryAdapter(Resource.usercategories, this);

        GridLayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 3);
        mLayoutManager.getPaddingLeft();
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(adapter);
        if (Resource.addactivity == true) {
            Resource.activities.add(Resource.activity);
            Resource.addactivity = false;

        } else if (Resource.isEdit == true) {
            Resource.activities.remove(Resource.position);
            Resource.activities.add(Resource.position, Resource.activity);
        }
        activityAdapter = new ActivityAdapter(Resource.activities, this);
        GridLayoutManager activityLayoutManager = new GridLayoutManager(getActivity(), 1);
        activityLayoutManager.getPaddingLeft();
        activityRecycleView.setLayoutManager(activityLayoutManager);
        activityRecycleView.setAdapter(activityAdapter);

        imgAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).replaceFragment(new CategoryFragment());
            }
        });
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String cat = "";
                for (int i = 0; i < Resource.usercategories.size(); i++) {
                    cat = cat + Resource.usercategories.get(i).getId() + ",";

                }

                if (guidePrice.getText().toString() == "" || guidePrice.getText().toString() == null || cat == "") {
                    Toast.makeText(getActivity(), "fill all field", Toast.LENGTH_SHORT).show();
                } else {
                    addprofile(cat);
                }


            }
        });

        imgAddActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Resource.activity = null;
                Resource.isEdit = false;
                ((MainActivity) getActivity()).replaceFragment(new EditActivityFragment());

            }
        });


        return view;
    }


    public void showaddImage() {
        imgAdd.setVisibility(View.VISIBLE);
        imgRemove.setVisibility(View.GONE);
    }

    public void showremoveImage() {
        imgAdd.setVisibility(View.GONE);
        imgRemove.setVisibility(View.VISIBLE);
    }


    public void addprofile(final String categories) {
        dialog = BaseActivity.ShowConstantProgressNOTCAN(getActivity(), "", getResources().getString(R.string.processing));
        dialog.show();

        final RequestQueue queue = VolleySingleton.getInstance(getActivity()).getmRequestQueue();
        String url = (Resource.BASE_URL + Resource.WHO + "/profile/add.json");
        StringRequest stringRequest_profile = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                dialog.dismiss();
                try {

                    JSONObject jsonObject = new JSONObject(response);  // response is a array come from API

                    boolean success = jsonObject.getBoolean("success");
                    if (success) {

                        ((MainActivity) getActivity()).replaceFragment(new MapViewFragment());


                    } else {
                        JSONObject err = jsonObject.getJSONObject("error");
                        String message = err.getString("description");
                        BaseActivity.showerror(getActivity(), message);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }


            //obj.register();

            // Toast.makeText(EWalletApplication.getContext(), " Register", Toast.LENGTH_SHORT).show();


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                dialog.dismiss();
                //showerror("not set gcm id on server");

                Toast.makeText(getActivity(),R.string.profile_not_added, Toast.LENGTH_SHORT).show();


            }

        }) {
            @Override
            public HashMap<String, String> getParams() {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("token", Resource.TOKEN);
                params.put("price", guidePrice.getText().toString());
                params.put("category", categories);
                params.put("latitude", "0.0");
                params.put("longitude", "0.0");
                params.put("description", "good");


                return params;
            }
        };

        stringRequest_profile.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(stringRequest_profile);


    }


    public void deletepachage(final int id) {
        dialog = BaseActivity.ShowConstantProgressNOTCAN(getActivity(), "", getResources().getString(R.string.processing));
        dialog.show();
        final RequestQueue queue = VolleySingleton.getInstance(getActivity()).getmRequestQueue();
        String url = (Resource.BASE_URL + "package/delete.json?id=" + id + "&token=" + Resource.TOKEN);
        StringRequest stringRequest_add_package = new StringRequest(Request.Method.DELETE, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                dialog.dismiss();

                try {
                    JSONObject jsonObject = new JSONObject(response);  // response is a array come from API

                    boolean success = jsonObject.getBoolean("success");
                    if (success) {

                        Resource.activities.remove(Resource.activity);
                        activityAdapter = new ActivityAdapter(Resource.activities,ActivityFragment.this);
                        GridLayoutManager activityLayoutManager = new GridLayoutManager(getActivity(), 1);
                        activityLayoutManager.getPaddingLeft();
                        activityRecycleView.setLayoutManager(activityLayoutManager);
                        activityRecycleView.setAdapter(activityAdapter);
                        activityAdapter.notifyDataSetChanged();
                        ((MainActivity) getActivity()).replaceFragment(new ActivityFragment());
                    } else {
                        JSONObject error = jsonObject.getJSONObject("error");
                        String message = error.getString("description");
                        BaseActivity.showerror(getActivity(), message);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


                //obj.register();

                // Toast.makeText(EWalletApplication.getContext(), " Register", Toast.LENGTH_SHORT).show();

            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                dialog.dismiss();
                //showerror("not set gcm id on server");

                Toast.makeText(getActivity(),R.string.delete_activity, Toast.LENGTH_SHORT).show();


            }

        }) {
            @Override
            public HashMap<String, String> getParams() {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("token", Resource.TOKEN);


                return params;
            }
        };

        stringRequest_add_package.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(stringRequest_add_package);

    }

}
