package io.itmatic.tracktrot.Adapters;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;

import java.util.List;

import io.itmatic.tracktrot.Model.Guide;
import io.itmatic.tracktrot.R;

public class GuideListAdapter extends RecyclerView.Adapter<GuideListAdapter.ContactViewHolder> { // recycledapter is a type of adapter  it also include recycle

        private List<Guide> guide_list;

        public GuideListAdapter(List<Guide> contactList) {
            this.guide_list = contactList;
        }

        @Override
        public int getItemCount() {
            return guide_list.size();
        }

        View.OnClickListener getClickListener(final int position) {
            return new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // ((MainActivity) getActivity()).replaceFragment(new MapViewFragment());
                    Guide service=guide_list.get(position);


                }
            };
        }



    @Override
        public void onBindViewHolder(ContactViewHolder contactViewHolder, int i) {
            Guide ci = guide_list.get(i);

           /* contactViewHolder.text_name.setText(ci.getServiceName());
            contactViewHolder.nv_icon.setImageUrl(ci.getServiceImageUrl(),mIMAGELOADER);
            contactViewHolder.cardView.setOnClickListener(getClickListener(i));
            contactViewHolder.cardView.setBackgroundColor(Color.parseColor(ci.getServiceColor()));
*/

            //set a category name on largeplayer

           // contactViewHolder.guideImage.setImageUrl(Resource.guides.get(i).getUrlImage(),);






        }

        @Override
        public ContactViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.guide_layout, viewGroup, false);

            return new ContactViewHolder(itemView);
        }

        public  class ContactViewHolder extends RecyclerView.ViewHolder {
            private NetworkImageView guideImage;
            private TextView name;
            private TextView profile;
            private CardView cardView;


            public ContactViewHolder(View v) {
                super(v);
                guideImage=(NetworkImageView) v.findViewById(R.id.guide_image);
                name=(TextView) v.findViewById(R.id.guide_name);
                cardView=(CardView) v.findViewById(R.id.cardview);


            }
        }

    }